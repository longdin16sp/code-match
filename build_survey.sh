#!/bin/bash
cd /var/www/html/magento/
rm -rf app/code/Survey
cp -r ~/Survey/ app/code/Survey

chown -R magento:www-data /var/www/html/magento
chmod 775 -R /var/www/html/magento

php bin/magento setup:upgrade
rm -rf var/cache/* var/page_cache/* generated/* var/view_prepocessed/*
#php bin/magento cache:clean && php bin/magento cache:flush
#php bin/magento setup:di:compile
