<?php

namespace Survey\SurveyPage\Controller\Index;

use Magento\Framework\App\Action\Context;
use Magento\Framework\Controller\ResultFactory; 
 
class Result extends \Magento\Framework\App\Action\Action
{
    protected $_resultPageFactory;
    protected $_objectManager;
    protected $_customerSession;
 
    public function __construct(
	Context $context, 
	\Magento\Framework\View\Result\PageFactory $resultPageFactory,
	\Magento\Framework\ObjectManagerInterface $objectManager,
	\Magento\Customer\Model\Session $customerSession
    )
    {
        $this->_customerSession = $customerSession;
	$this->_objectManager = $objectManager;
        $this->_resultPageFactory = $resultPageFactory;
        parent::__construct($context);
    }
 
    public function execute()
    {
        $errors = [];
				
	// Get the posted data
	$post = $this->getRequest()->getPostValue();
				
	// check name
	if (isset($post['name'])) {
            if (preg_match('/[\w]+/',$post['name'])) {
		$name=$post['name'];
            } else {
                $errors[] = 'Check your name';
            }
	} else {
            $errors[] = 'Missing name';
	}
        // Check Email
        if (isset($post['emails'])) {
            if(filter_var($post['emails'], FILTER_VALIDATE_EMAIL))
            {
		$email=$post['emails'];
            } else {
                $errors[] = 'Check your email';
            }
	} else {
            $errors[] = 'Missing email';
	}
        // Check comment
	if (isset($post['cmt'])) {
            if (preg_match('/[\w]+/',$post['cmt'])) {
                $cmt=$post['cmt'];
            } else {
                $errors[] = 'Check your comment';
            }
	} else {
            $errors[] = 'Missing comment';
	}

	// Check recommended
	if (isset($post['recommend'])) {
            $recommend = ($post['recommend'] == 'on');
	} else {
            $recommend = false;
	}
				
	// Check best
	if (isset($post['best'])) {
            if (preg_match('/[\w]+/',$post['best'])) {
		$best=$post['best'];
            } else {
		$errors[] = 'Select a product';
            }
	} else {
            $errors[] = 'Missing product';
	}
				
				
	if (!empty($errors)) {
            $this->_customerSession->setMyValue($errors);

            
            $resultRedirect = $this->resultRedirectFactory->create();
            $resultRedirect->setPath('survey/index/');
            return $resultRedirect;
	}
			
	// Storing via model to database
	$model = $this->_objectManager->create('Survey\SurveyPage\Model\Post');
	$model->setData('custname', $name);
        $model->setData('emails',$email);
        $model->setData('cmt', $cmt);
	$model->setData('bestproduct', $best);
	$model->setData('recommend', ($recommend ? 1:0));					
	$model->save();
				
        $resultPage = $this->_resultPageFactory->create();
	return $resultPage;
    }
}