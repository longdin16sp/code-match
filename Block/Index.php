<?php
namespace Survey\SurveyPage\Block;

class Index extends \Magento\Framework\View\Element\Template
{
    protected $_productCollectionFactory;
    protected $_customerSession;
	
    public function __construct(
		\Magento\Framework\View\Element\Template\Context $context,
		\Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productCollectionFactory,
		\Magento\Customer\Model\Session $customerSession
    )
    {
        $this->_productCollectionFactory = $productCollectionFactory;
        $this->_customerSession = $customerSession;
	parent::__construct($context);
    }

    public function getErrors() {
       
	$tmp = $this->_customerSession->getMyValue();
        $this->_customerSession->setMyValue('');
	return $tmp;
	}

    public function getProducts()
    {
	$collection = $this->_productCollectionFactory->create();
	$collection->addAttributeToSelect('*');
	return $collection;
    }
}
